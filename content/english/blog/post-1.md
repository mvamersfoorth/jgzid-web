---
date: "2021-02-28"
title: "5 grondslagen voor een optimale gebruikersbeleving in de zorg [webinar]"
image: "images/blog/grondslagen.jpg"
author: "leanne-kempees"
draft: false
---

>Zorg gaat over mensen. De digitale versnelling die deze tijd van deze sector vraagt, maakt de gebruikerservaring voor een digitaal platform nog belangrijker. Want een platform dat geen aansluiting vindt bij mensen, is weggegooid geld. Finalist overbrugt al jaren het gat tussen de ‘business’ en de ‘gebruiker’ en de 5 meest belangrijke elementen delen we graag voor een betere en betaalbare zorg.
>

#### Drie key-learnings

- Kennis over hoe aansluiting te vinden bij eindgebruikers.
- Analyse van bijzonderheden in het zorgdomein.
- Inzichten over optimale gebruikerservaring door echte voorbeelden.

#### Webinar on demand (30 minuten)

Hoor de echte feiten in dit gesprek tussen Léanne Kempees en Jasper van Schelven uit onze ervaring met softwaretrajecten voor zorgorganisaties. Jarenlange samenwerking met zorgprofessionals heeft ons waardevolle inzichten gegeven voor optimale gebruikerservaringen die we delen in dit 30 minuten durende webinar.

#### Voor wie?

Voor digitale leiders die betrokken zijn bij software projecten voor zorginstellingen.


[Aanmelden webinar](https://www.finalist.nl/actueel/evenementen/5-grondslagen-voor-een-optimale-gebruikersbeleving-de-zorg-webinar)