---
date: "2021-05-11"
title: "Hoe ICT de zorg kan ontzorgen? Niet belemmeren, maar verlichten!"
image: "images/blog/iStock-155751694_high.jpg"
author: "samantha-boer"
draft: false
---

>Organiseer de ict-systemen op zo’n manier dat ze ondersteunend werken voor de zorgprofessional
>
> <cite>Samantha Boer, accountmanager bij Finalist</cite>

“Wat je in de breedste zin in de zorgmarkt ziet is, is dat digitale oplossingen een fantastisch middel zijn om de zorg te faciliteren. Alleen moeten we soms even een stapje terug doen om te bekijken of het systeem die ondersteuning ook echt biedt. Nu zie je soms gebeuren dat zorgprofessionals eerder last hebben van de applicaties en de techniek die hen zouden moeten helpen. Vaak worden besluiten voor een systeem of applicatie genomen op basis van financiële of andere redenen, anders dan het dienen van de zorg. De software die wij bouwen is bedoeld voor de eindgebruikers, voor de zorgprofessional dus. En daarom beginnen wij altijd met een traject waarbij we weten voor wie we het doen, wij kennen onze eindgebruikers en gaan in gesprek om echt te begrijpen wat ze nu nodig hebben. Vervolgens maken wij de applicatie gebruiksvriendelijk en intuïtief.”

#### Vergrijzing

“De gemiddelde zorgprofessional heeft voor de zorg gekozen vanuit zijn of haar hart. Je ziet ook dat er vergrijzing is onder de zorgprofessionals; veel huisartsen en thuiszorgmedewerkers zijn bijvoorbeeld al wat ouder. Die mensen ervaren ict-systemen nu als hinder; er wordt een bepaalde technische kennis verwacht die niet iedereen heeft. Facebook snapt iedereen; dit is een zeer gebruiksvriendelijk systeem. Voor veel ict-systemen die zorgpersoneel gebruikt, geldt dit niet. Een systeem dat niet ondersteunend werkt, daarvoor is weinig draagvlak. Het succes van een goed systeem ligt voor een belangrijk deel in de adoptie onder professionals. Ook co-creatie is een belangrijke succesfactor in onze projecten. Samenwerken binnen het project met de eindgebruikers zien wij als essentieel.”

“Praktisch voorbeeld: als je als verpleegkundige je ronde wil doen in een verpleegtehuis, zul je eerst in een systeem moeten kijken wat je planning is, in een ander systeem bekijk je de status van de cliënten. In wéér een ander systeem registreer je je urenverantwoording en weer op een andere plek zie je wie er jarig is en welke coronamaatregelen daar nu voor gelden. En dan heb je nog niet eens je koffie op.” Het is dus erg belangrijk, om naast gebruiksvriendelijkheid en het echt kennen van de gebruikers, ook veel aandacht te hebben voor het trainen en ontwikkelen van digitale vaardigheden van de zorgprofessionals.”

#### Niet belemmeren, maar verlichten

Het omslachtige karakter van die verschillende systemen en verplichtingen kunnen bij de mensen weerstand oproepen. “Tegelijkertijd beseffen mensen ook niet altijd dat we die systemen nodig hebben. En hoe belangrijk bijvoorbeeld die urenverantwoording is om geld te kunnen claimen bij de zorgverleners. Kort gezegd: De zorg heeft die systemen nodig en we zullen ze zo moeten maken en inzetten dat ze het werk verlichten in plaats van belemmeren.” Een mensgerichte aanpak waarbij we echt weten wat de professional nodig heeft alvorens we software gaan bouwen, alleen dan krijgen we systemen die voor de zorg werken.

#### In één oogopslag

Samantha: “Organiseer ict zó dat ze in dienst staat van de zorg. Zodat je in één oogopslag en in één systeem kunt zien - afhankelijk van je rechten en rollen - wat de planning is, dat de collega van de kantine jarig is, en wat de staat is van de cliënten die je met zoveel liefde verzorgt. Zo creëer je een vriendelijke, persoonlijke werkomgeving die helpend is en ben je minder tijd kwijt aan administratieve lasten.  Dan kun je die vijf minuten die je nu extra hebt, besteden aan het feliciteren van die kleinzoon van die cliënt. Die persoonlijke gesprekken - waar nu vaak geen tijd meer voor is - vormen juist zo’n belangrijk onderdeel van het werken in de zorg. Door de juiste inzet van ict-systemen, kunnen zorgmedewerkers weer het werk doen waar ze oorspronkelijk voor hebben gekozen: zorg verlenen met oog voor de mens.”

“Maak gebruik van wat er reeds is: van bestaande applicaties die vaak veel waardevolle informatie bevatten en van de mensen die precies weten wat nodig is voor de cliënten. Integreer bestaande systemen zodat data op één plek toegankelijk wordt in combinatie met de kennis van de zorgprofessional. Dan creëer je echte waarde!”