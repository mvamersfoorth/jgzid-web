---
title: "Over JGZiD"
layout: "about"
draft: false

# who_we_are
who_we_are:
  enable: true
  subtitle: "Wie wij zijn"
  title: "Over JGZiD"
  description: "Het JGZiD is hét digitaal dossier voor een gezonde toekomst: ontwikkeld vóór en dóór professionals met hart voor de jeugdgezondheidszorg. Het JGZiD is een digitaal dossier op maat voor de jeugdgezondheidszorg. Een systeem dat de professional ondersteunt bij de taakuitvoering en organisaties in staat stelt om snel relevante informatie te delen met haar ketenpartners."

  image: "images/about/iStock-607035261_super.jpg"

# what_we_do
what_we_do:
  enable: true
  subtitle: "Hartslag"
  title: "De kern van het systeem"
  block:
  - title: "Gebruiksvriendelijk"
    content: "Een digitaal dossier heeft verschillende gebruikers met allen een eigen informatie behoeften. Met het JGZiD presenteren we een mensgerichte, intuïtieve en slimme oplossing voor de Jeugd Gezondheid Zorg. Het dossier biedt schermen die gericht zijn op het vast leggen van enkel relevante gegevens, passende bij de patiënt die de zorgprofessional voor zich heeft. Binnen het JGZiD staat de patiënt centraal, als eigenaar van de eigen gezondheidgegevens. Via de wettelijke normen en certificeringen worden data met ketenpartners gedeeld zoals gemeenten en RIVM. Overdracht van medische gezondheidsgegevens en dossier binnen de zorgketen door een logische, uniforme manier van vastlegging bevorderd de zorgkwaliteit."

  - title: "Regie ligt bij JGZ"
    content: "Het JGZiD is configurabel. Hierdoor zijn JGZ-organisaties zelf in staat om het digitale dossier aan te passen aan het zorgproces van de organisatie. Het JGZiD is een toekomstbestendig, open platform gebaseerd op de meest moderne architectuurprincipes en bewezen technologieën. Hierbij blijft de eigen identiteit van een JGZ-organisatie altijd behouden doordat we de huisstijl van de organisatie  integreren. Met de flexibele inrichting kan elke JGZ-organisatie via de eigen IT afdeling zelf gemakkelijk eigen werkprocessen en user interfaces op maat inrichten."
    
  - title: "Ondersteunen van de zorgprofessional"
    content: "Het JGZiD is een digitaal dossier op maat voor de jeugdgezondheidszorg. Een systeem dat de professional ondersteunt bij de taakuitvoering en organisaties in staat stelt om snel relevante informatie te delen met haar ketenpartners."
    
  - title: "VZVZ gekwalificeerd"
    content: "JGZiD is door VZVZ gekwalificeerd als ‘Goedbeheerd Zorgsysteem’ en stelt professionals in staat om intuïtief en adequaat alle relevante gegevens van het kind en van de jongere vast te leggen. Ook biedt het ouders op een gebruiksvriendelijke en veilige manier informatie over hun kind."

  - title: "Modulair"
    content: "De basis van JGZiD is modulair. Dit betekent dat een JGZ organisatie datgene kan gebruiken wat past bij het eigen zorgproces en zo niet onnodige betaald voor functionaliteiten die niet gebruikt worden. Door de modulaire opbouw is uitbreiding altijd mogelijk zonder dat de applicatie of data in gevaar komen. Via een integratielaag ontsluiten bestaande data vanuit reeds bestaande applicaties waardoor er binnen het bestaande architectuurlandschap geen aanpassing nodig zijn om gebruik te maken van het digitale dossier."

  - title: "JGZiD is de toekomst"
    content: "De gekozen architectuur, open standaarden en modulaire opbouw maakt het dossier flexibel, schaalbaar en boven uitermate geschikt voor doorontwikkeling. Doorontwikkeling op functioneel gebied maar ook inspelend op wijziging in beleid en wetswijzigingen. Doorontwikkeling is het resultaat van de nauwe samenwerking met gebruikers uit de JGZ. Met de zorg, vóór de zorg!"

# our_mission
our_mission:
  enable: true
  subtitle: "Over de maker"
  title: "Product van Finalist"
  description: "Van digitalisering naar digitale transformatie in de zorg. Finalist kan hierbij helpen. Dat doen wij bijvoorbeeld met het digitaal dossier voor de jeugdgezondheidszorg: het JGZiD. Een digitaal dossier dat de zorgprofessional optimaal ondersteunt bij het uitvoeren van zijn taken op basis van de zorgprocessen. Met het JGZiD wordt een organisatie flexibel, wendbaar en toekomstbestendig."

  image: "images/about/iStock-155751694_high.jpg"

# about_video
about_video:
  enable: true
  subtitle: "Onze missie"
  title: "Onze wereld toegankelijker maken"
  description: "Met het JGZiD maken wij de wereld toegankelijker. Niet voor niets is het onze missie: ieder individu heeft recht op een digitale ervaring die het welzijn vergroot."
  video_url: "https://www.youtube.com/embed/g0krMGRZnJw"
  video_thumbnail: "images/about/video-popup-2.jpg"


# clients
clients_carousel:
  enable: true
  subtitle: "Onze klanten"
  title: "Onze vele klanten"
  # section: "/" # client images comming form _index.md
  client_images:
  - images/clients/01-colored.png
  - images/clients/02-colored.png
  - images/clients/03-colored.png
  - images/clients/05-colored.png
  - images/clients/04-colored.png
  - images/clients/06-colored.png
  - images/clients/07-colored.png

# our team
our_team:
  enable: true
  subtitle: "Ons team"
  title: "Onze specialisten"
  description: "Dit zijn onze JGZiD specialisten die je graag verder helpen."
  team:
  - name: "Edwin Versteegt"
    image: "images/about/team/collegas.png"
    designation: "Enterprise Architect"


# our office
our_office:
  enable: true
  subtitle: "Onze locaties"
  title: "Onze locaties"
  description: "Finalist creëert moderne digitale ervaringen voor belangrijke levensfasen. De 90 hoogopgeleide collega’s werken met passie aan moderne en gebruikersvriendelijke websites, portals, applicatie-integratie en maatwerk applicaties voor meer dan 1.000.000 eindgebruikers in Nederland."
  office_locations:
  - city: "Rotterdam"
    address_line_one: "Weena 690"
    address_line_two: "3012 CN Rotterdam"
  - city: "Maarssen"
    address_line_one: "Safariweg 73"
    address_line_two: "3605 MA Maarssen"
---