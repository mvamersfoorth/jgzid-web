---
banner:
  title: Nieuwste generatie digitaal kinddossier
  button: Plan nu de demo
  button_link: "signup/"
  image: images/banner-app.png
brands_carousel:
  enable: true
  brand_images:
  - images/brands/01-colored.png
  - images/brands/02-colored.png
  - images/brands/03-colored.png
  - images/brands/05-colored.png
  - images/brands/04-colored.png
  - images/brands/06-colored.png
  - images/brands/07-colored.png
clients_carousel:
  enable: true
  client_images:
  - images/clients/01-colored.jpg
  - images/clients/02-colored.png
  - images/clients/03-colored.png
  - images/clients/04-colored.png
  - images/clients/05-colored.png
features:
  enable: true
  subtitle: Producteigenschappen
  title: Het beste digitaal <br> kinddossier ooit
  description: JGZiD is voor en door de jeugdgezondheidszorg ontwikkeld, zodat meer tijd beschikbaar is voor echte zorg.
  features_blocks:
  - icon: las la-user
    title: Gebruiksvriendelijk
    content: JGZiD biedt een gebruikersvriendelijk dossier voor alle gebruikers. Een digitaal dossier heeft verschillende gebruikers met allen een eigen informatiebehoeften. Met het JGZiD presenteren we een mensgerichte, intuïtieve en slimme oplossing voor de Jeugd Gezondheid Zorg.
  - icon: las la-cubes
    title: Configurabel
    content: JGZiD is configurabel en flexibel. Dit betekent concreet dat het dossier zich aanpast aan de zorgprocessen van de professionals en niet anders om! JGZiD is ondersteunend en staat volledig in dienst van de zorg, van de professional en van de organisatie.
  - icon: las la-users
    title: Co-creatie
    content: Samenwerking binnen de zorg is belangrijk. Wij geloven in de kracht van samenwerking en innovatie. De wensen van de JGZ en de gebruikers staan hierbij centraal. Dat resulteert in een open en flexibel digitaal dossier dat klaar is voor de toekomst.
  - icon: las la-building
    title: Doorontwikkeling
    content: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque enim id
      diam ornare volutpat in sagitis, aliquet. Arcu cursus
  - icon: las la-home
    title: Ouderportaal
    content: Het dossier biedt een gebruikersvriendelijk portaal waarin kind informatie voor ouders toegankelijk gemaakt wordt. Ouders hebben zo inzicht in de medische informatie, voortgang en ontwikkeling van hun kind.
  - icon: las la-wrench
    title: Koppelingen
    content: Het dossier koppelt met bestaande systemen zoals ERP, HRM, Rooster & Planning en het LSP. Iedere JGZ-organisatie behoudt hiermee de eigen bestaande applicaties en maakt gebruik van reeds aanwezige informatie en data.
  - icon: las la-random
    title: Dossieroverdracht
    content: Met het JGZiD kan eenvoudig gegevens overgedragen worden. Niet alleen binnen de eigen organisatie, maar juist ook met gemeenten, ketenpartners en cliënten. Dit door de inzet van open technologieën die gemakkelijk, volledig veilig, informatie met elkaar kunnen uitwisselen.
intro_video:
  enable: true
  subtitle: Dit Is Waarom
  title: Speciaal gemaakt voor jou
  description: Door goed te luisteren hebben we het JGZiD product ontwikkeld. Het motiveert ons enorm dat we op deze manier bijdragen aan onze samenleving.
  video_url: https://www.youtube.com/embed/H8WpN8c-gNk
  video_thumbnail: images/video-popup-3.jpg
how_it_works:
  enable: true
  section: how-it-works
testimonials:
  enable: true
  subtitle: Referenties
  title: Hoor het vooral van anderen
  description: Geloof ons niet op ons woord, maar vertrouw onze loyale opdrachtgevers in hun oordeel. 
  image_left: images/testimonials-01.png
  image_right: images/testimonials-02.png
  testimonials_quotes:
  - quote: Ik heb Finalist leren kennen als een prettige technische partner met hart voor zowel softwareontwikkeling als voor de zorg. Ze zijn flexibel, transparant en verstaan hun vak goed.
    name: Gert Koelewijn
    designation: Nictiz
    image: images/user-img/gert.png
  - quote: Als ik mij bedenk hoe de samenwerking met Finalist verloopt dan kan ik daar eenvoudig op antwoorden - transparantie, gewoon doen wat afgesproken is en geen zorgen voor mij als klant.
    name: Maarten Fischer
    designation: Nederlandse Vereniging van Ziekenhuizen
    image: images/user-img/maarten.png
  - quote: We hebben de samenwerking met Finalist hierin als zeer prettig ervaren, bevlogen team van medewerkers met adequate kennis van software maar zeker ook betrokken bij de inhoud van ons programma.
    name: Daniëlle van Sambeek
    designation: GGD GHOR
    image: images/user-img/danielle.png
---
