---
title: "Hoe het werkt"
layout: "how-it-works"
draft: false

how_it_works_video:
  enable: true
  subtitle: "Producteigenschappen"
  title: "Hoe het werkt"
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi egestas <br> Werat viverra id et aliquet. vulputate egestas sollicitudin."
  video_url: "https://www.youtube.com/embed/dyZcRRWiuuw"
  video_thumbnail: "images/video-popup.jpg"


# how_it_works
how_it_works:   
  enable: true
  block:
  - subtitle: "Voor JGZ-professionals"
    title: "Zeg hallo tegen één scherm"
    description: "Alle relevante informatie vindbaar in één scherm. Intensieve samenwerking (co-creatie) met zorgverleners uit de jeugdgezondheidszorg zorgt voor het beste overzicht ooit.."
    image: "images/features-01.png"

  - subtitle: "Voor functioneel beheerders"
    title: "Jij weet het beter"
    description: "Goed nieuws voor controlfreaks. JGZiD is te configureren naar wens. Pas het product aan naar jouw proces, huisstijl en rechtenstructuur."
    image: "images/features-02.png"

---