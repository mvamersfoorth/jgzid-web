---
title: "Samantha Boer"
image: "images/author/samantha.jpg"
email: "samantha.boer@finalist.nl"
social:
  - icon : "lab la-linkedin-in"
    name: "linkedin"
    link : "https://www.linkedin.com/in/samanthaboer/"
---

Samantha Boer is accountmanager bij Finalist.