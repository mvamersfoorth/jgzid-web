---
title: "Léanne Kempees"
image: "images/author/leanne.png"
email: "leanne.kempees@finalist.nl"
social:
  - icon : "lab la-linkedin-in"
    name: "linkedin"
    link : "https://www.linkedin.com/in/leannekempees/"
---

Léanne Kempees is business unit manager Zorg & Zekerheid van Finalist.